import { ConfigFileService } from './../services/config-file.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private apiPath: string;

  constructor(
    private _http: HttpClient,
    private _cfgSrvc: ConfigFileService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(() => {
      this.apiPath = this._cfgSrvc.getApiPath();
      console.log('API Path: ', this.apiPath);
    });
  }

  public login(username: string, password: string) {
    return this._http.get(this.apiPath + `login?adminid=${username}&pass=${password}`);
  }
}
