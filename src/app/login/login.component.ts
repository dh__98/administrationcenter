import { SnackbarService, EMode } from './../snackbar.service';
import { LoginService } from './login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [

  ]
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;

  constructor(
    private _router: Router,
    public dialog: MatDialog,
    private _loginService: LoginService,
    private _snackbarService: SnackbarService
  ) { }

  ngOnInit() {
  }

  public validateCredentials() {
    this._loginService.login(this.username, this.password).subscribe(result => {
      this._router.navigateByUrl('home');
      this._snackbarService.create('Willkommen.', 'Ok', EMode.info);
    }, error => {
      this._snackbarService.create('Anmeldedaten fehlerhaft.', 'Ok', EMode.warn);
    });
  }

  public checkKeyPressed(event) {
    if (event.code === 'Enter') {
      this.validateCredentials();
    }
  }

}
