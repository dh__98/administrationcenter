export class UserGroupModel {
    id: number;
    name: string;
    access_level: number;
}
