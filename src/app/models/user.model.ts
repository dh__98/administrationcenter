export class UserModel {
    id: number;
    name: string;
    lastname: string;
    taguid: string;
    // Confirmed: boolean;
    accesslevel: number;
    birthdate: Date;
    user_group_id: number;
    user_group_name: string;
}
