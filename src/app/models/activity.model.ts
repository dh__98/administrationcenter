export class ActivityModel {
    ID: number;
    TimeStamp: Date;
    UserName: string;
    LockDescription: string;
}
