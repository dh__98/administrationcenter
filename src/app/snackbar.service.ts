import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';

export enum EMode {
  warn = 1,
  info = 2
}

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(
    private snackBar: MatSnackBar
  ) { }

  public create(message: string, action: string, mode: EMode) {
    let duration = 0;
    let panelClass = '';
    if (mode === EMode.warn) {
      panelClass = 'warn-snackbar';
      duration = 5000;
    } else if (mode === EMode.info) {
      panelClass = 'info-snackbar';
      duration = 3000;
    }
    this.snackBar.open(message, action, {
      duration: duration,
      panelClass: [panelClass]
    });
  }

}
