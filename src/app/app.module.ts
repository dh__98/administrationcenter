
// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ActivitiesComponent } from './home/activities/activities.component';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './home/users/users.component';
import { LocksComponent } from './home/locks/locks.component';
import { UserGroupsComponent } from './home/user-groups/user-groups.component';
import { UserDialogComponent } from './home/users/user-dialog/user-dialog.component';
import { SettingsDialogComponent } from './dialogs/settings-dialog/settings-dialog.component';
import { InfoDialogComponent } from './dialogs/info-dialog/info-dialog.component';
import { LockDialogComponent } from './home/locks/lock-dialog/lock-dialog.component';
import { UserGroupDialogComponent } from './home/user-groups/user-group-dialog/user-group-dialog.component';
import { ModularDialogComponent } from './dialogs/modular-dialog/modular-dialog.component';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { HttpService } from './services/http.service';
import { ConfigFileService } from './services/config-file.service';

// Angular Material
import {
  MatButtonModule,
  MatGridListModule,
  MatListModule,
  MatCardModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatTabsModule,
  MatDialogModule,
  MatStepperModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatAutocompleteModule
} from '@angular/material';

// PrimeNG
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { DataGridModule } from 'primeng/datagrid';

// Fontawesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';


library.add(fas);


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ActivitiesComponent,
    HomeComponent,
    UsersComponent,
    LocksComponent,
    UserGroupsComponent,
    UserDialogComponent,
    SettingsDialogComponent,
    InfoDialogComponent,
    LockDialogComponent,
    UserGroupDialogComponent,
    ModularDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ButtonModule,
    CardModule,
    DataGridModule,
    FontAwesomeModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    MatListModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatTabsModule,
    FormsModule,
    MatDialogModule,
    MatStepperModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  exports: [
    MatSortModule,
    MatPaginatorModule
  ],
  entryComponents: [
    UserDialogComponent,
    SettingsDialogComponent,
    InfoDialogComponent,
    LockDialogComponent,
    UserGroupDialogComponent,
    ModularDialogComponent
  ],
  providers: [
    HttpService,
    ConfigFileService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
