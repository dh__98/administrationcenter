import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModularDialogData } from 'src/app/home/users/user-dialog/user-dialog.component';

export class ModularData {

  header: string;
  message: string;
  buttonTxtTrue: string;
  buttonTxtFalse: string;

  constructor(
    header: string,
    message: string,
    buttonTxtFalse: string,
    buttonTxtTrue: string
  ) {
    this.header = header;
    this.message = message;
    this.buttonTxtFalse = buttonTxtFalse;
    this.buttonTxtTrue = buttonTxtTrue;
  }
}

@Component({
  selector: 'app-modular-dialog',
  templateUrl: './modular-dialog.component.html',
  styleUrls: ['./modular-dialog.component.css']
})
export class ModularDialogComponent implements OnInit {

  public data: ModularData;

  constructor(
    public dialogRef: MatDialogRef<ModularDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: ModularDialogData,
  ) {
    console.log('dialogData.modularData: ', dialogData.modularData);
    this.data = dialogData.modularData;
    console.log('this.data: ', this.data);
  }

  ngOnInit() {
  }

  public returnTrue() {
    this.dialogRef.close(true);
  }

  public returnFalse() {
    this.dialogRef.close(false);
  }

}
