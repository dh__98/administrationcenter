

import {
    trigger,
    animate,
    transition,
    style,
    query,
    stagger,
    state
} from '@angular/animations';

export const fadeAnimation = trigger('fadeAnimation', [
    // The '* => *' will trigger the animation to change between any two states
    transition('* => *', [
        // The query function has three params.
        // First is the event, so this will apply on entering or when the element is added to the DOM.
        // Second is a list of styles or animations to apply.
        // Third we add a config object with optional set to true, this is to signal
        // angular that the animation may not apply as it may or may not be in the DOM.
        query(
            ':enter',
            [style({ opacity: 0, transform: 'translateY(-2000px)' })],
            { optional: true }
        ),
        query(
            ':leave',
            // here we apply a style and use the animate function to apply the style over 0.3 seconds
            [style({ opacity: 1 }), animate('0.3s', style({ opacity: 0 }))],
            { optional: true }
        ),
        query(
            ':enter',
            [style({ opacity: 0 }), animate('0.5s', style({ opacity: 1 }))],
            { optional: true }
        )
    ])
]);

export const filterAnimation = trigger('filterAnimation', [
    transition(':enter, * => 0, * => -1', []),
    transition(':increment', [
        query(':enter', [
            style({ opacity: 0, width: '0px' }),
            stagger(50, [
                animate('300ms ease-out', style({ opacity: 1, width: '*' })),
            ]),
        ], { optional: true })
    ]),
    transition(':decrement', [
        query(':leave', [
            stagger(50, [
                animate('300ms ease-out', style({ opacity: 0, width: '0px' })),
            ]),
        ])
    ]),
]);

export const flyInAnimation = trigger('EnterLeave', [
    state('flyIn', style({ transform: 'translateX(0)' })),
    transition(':leave', [
        animate('0.25s ease-out', style({ transform: 'translateX(80%)', opacity: 0}))
    ]),
    transition(':enter', [
        style({ transform: 'translateX(-200%)', opacity: 0 }),
        animate('0.2s ease-in')
    ])
]);
