import { HttpModule } from '@angular/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  text: string;
  public stickyMargin: string;
  public sticky: boolean;

  constructor() { }

  ngOnInit() {
  }

  public scrollEvent(event) {
    // console.log('THEY SEE ME SCROLLIN, THEY HATIN, THEY CATCH ME CODIN, DIRTY TRY TO CATCH ME CODIN DIRTY');
    if (event.pageY < 70 && event.pageY >= 0) {
      this.stickyMargin = 71 - event.pageY + 'px';
      this.sticky = false;
    } else if (event.pageY >= 70) {
      this.sticky = true;
    }
  }

}
