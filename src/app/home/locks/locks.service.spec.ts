import { TestBed } from '@angular/core/testing';

import { LocksService } from './locks.service';

describe('LocksService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocksService = TestBed.get(LocksService);
    expect(service).toBeTruthy();
  });
});
