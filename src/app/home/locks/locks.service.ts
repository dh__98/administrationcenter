import { LockModel } from './../../models/lock.model';
import { ConfigFileService } from './../../services/config-file.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocksService {

  private apiPath: string;

  constructor(
    private _http: HttpClient,
    private _cfgSrvc: ConfigFileService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(() => {
      this.apiPath = this._cfgSrvc.getApiPath();
      console.log('API Path: ', this.apiPath);
    });
  }

  public getLocks() {
    return this._http.get(this.apiPath + 'locks');
  }

  public setLock(lock: LockModel) {
    return this._http.post(this.apiPath + 'locks/update', lock);
  }

  public deleteLock(id: number) {
    return this._http.delete(this.apiPath + 'locks/delete?id=' + id);
  }
}
