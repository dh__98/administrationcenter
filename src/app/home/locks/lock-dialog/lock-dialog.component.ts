import { ModularDialogComponent } from './../../../dialogs/modular-dialog/modular-dialog.component';
import { ModularData } from 'src/app/dialogs/modular-dialog/modular-dialog.component';
import { SnackbarService, EMode } from './../../../snackbar.service';
import { DialogData } from './../locks.component';

import { LockModel } from './../../../models/lock.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-lock-dialog',
  templateUrl: './lock-dialog.component.html',
  styleUrls: ['./lock-dialog.component.css']
})
export class LockDialogComponent implements OnInit {

  public lock: LockModel;
  public tempName: string;
  public levels: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  public isNew: boolean;

  constructor(
    public dialogRef: MatDialogRef<LockDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackbarService: SnackbarService,
    public dialog: MatDialog
  ) {
    if (data.lock.id) {
      this.lock = {
        accesslevel: data.lock.accesslevel,
        description: data.lock.description,
        id: data.lock.id
      };
    } else {
      this.lock = new LockModel();
      this.isNew = true;
    }
  }

  public setAccessLevel(level) {
    this.lock.accesslevel = level;
  }

  public saveLock() {
    if (this.lock) {
      if (!this.lock.accesslevel || !this.lock.description) {
        this.snackbar();
      } else {
        this.dialogRef.close(this.lock);
      }
    } else {
      this.snackbar();
    }
  }

  private snackbar() {
    this._snackbarService.create('Bitte alle Datenfelder füllen.', 'Ok', EMode.info);
  }

  public cancel() {
    this.dialogRef.close();
  }

  public deleteLock() {
    const modularData = new ModularData('Löschen', 'Durch das Löschen kann eine Neukonfiguration des Schlossen nötig werden.' +
      '\r\n Wollen Sie den Eintrag wirklich löschen?', 'Abbrechen', 'Ja');

    const dialogRef = this.dialog.open(ModularDialogComponent, {
      data: { modularData: modularData }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  ngOnInit() {
  }

}
