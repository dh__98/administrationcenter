import { LockDialogComponent } from './lock-dialog/lock-dialog.component';
import { LockModel } from './../../models/lock.model';
import { LocksService } from './locks.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { ConfigFileService } from 'src/app/services/config-file.service';
import { flyInAnimation, filterAnimation } from 'src/app/animations/animations';
import { SnackbarService, EMode } from 'src/app/snackbar.service';

export interface DialogData {
  lock: LockModel;
}

@Component({
  selector: 'app-locks',
  templateUrl: './locks.component.html',
  styleUrls: ['./locks.component.css'],
  animations: [
    flyInAnimation,
    filterAnimation
  ]
})
export class LocksComponent implements OnInit {

  displayedColumns: string[] = ['id', 'description', 'accesslevel'];
  public lockList: MatTableDataSource<LockModel>;
  public userTotal: number;
  public locksTotal: number;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _locksService: LocksService,
    public dialog: MatDialog,
    private _cfgSrvc: ConfigFileService,
    private _snackbarService: SnackbarService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(res => {
      setTimeout(() => {
        this._locksService.getLocks().subscribe(locks => {
          const tempLockList = locks as LockModel[];
          this.lockList = new MatTableDataSource(tempLockList);
          this.lockList.sort = this.sort;
          this.setLockCardHeigth(this.lockList.data.length);
        });
      }, 50);
    });
  }

  ngOnInit() {

  }

  public filterLocksList(criteria: string) {
    this.lockList.filter = criteria.trim().toLowerCase();
    if (this.lockList.paginator) {
      this.lockList.paginator.firstPage();
    }
    const newTotal = this.lockList.data.length;
    if (this.locksTotal !== newTotal) {
      this.userTotal = newTotal;
    } else if (!criteria) {
      this.locksTotal = -1;
    }
    this.setLockCardHeigth(this.lockList.filteredData.length);
  }

  public openLockDialog(lm: LockModel): void {
    let lock: LockModel = new LockModel();
    if (lm) {
      lock = lm;
    }
    const dialogRef = this.dialog.open(LockDialogComponent, {
      data: { lock: lock }
    });
    dialogRef.afterClosed().subscribe(result => {
      const tempLock: LockModel = result;
      if (tempLock) {
        if (tempLock.id && !tempLock.description && !tempLock.accesslevel) {
          this.deleteLock(tempLock.id);
        } else {
          this.saveLock(tempLock);
        }
      }
    });
  }

  private saveLock(lock: LockModel) {
    this._locksService.setLock(lock).subscribe(result => {
      if (lock.id) {
        this._snackbarService.create('Schloss erfolgreich gespeichert.', 'Ok', EMode.info);
      } else {
        console.log('result id: ', result[0].id);
        lock.id = result[0].id;
        this.lockList.data.push(lock);
        this.setLockCardHeigth(this.lockList.data.length);
        this._snackbarService.create('Schloss erfolgreich hinzugefügt.', 'Ok', EMode.info);
      }
    }, error => {
      this._snackbarService.create('Speichern fehlgeschlagen!', 'Ok', EMode.warn);
    });
  }

  private deleteLock(id: number) {
    this._locksService.deleteLock(id).subscribe(() => {
      this._snackbarService.create('Schloss erfolgreich gelöscht.', 'Ok', EMode.info);
    }, error => {
      this._snackbarService.create('Löschen fehlgeschlagen!', 'Ok', EMode.warn);
    });
  }

  private setLockCardHeigth(displayedRows: number) {
    setTimeout(() => {
      const lockcard = document.getElementById('lock-card-identifier');
      const tablecontainer = document.getElementById('locks-table-container');

      let lli_offsetHeight = 0;

      if (displayedRows > 12) {
        lli_offsetHeight = 650;
        tablecontainer.style.overflow = 'auto';
      } else {
        lli_offsetHeight = (displayedRows * 48) + 56;
        tablecontainer.style.overflow = 'hidden';
      }

      tablecontainer.style.height = (lli_offsetHeight).toString() + 'px';
      this.lockList.sort = this.sort;
    }, 100);
  }
}
