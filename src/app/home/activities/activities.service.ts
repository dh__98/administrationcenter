import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigFileService } from 'src/app/services/config-file.service';

@Injectable({
  providedIn: 'root'
})
export class ActivitiesService {

  private apiPath: string;

  constructor(
    private _http: HttpClient,
    private _cfgSrvc: ConfigFileService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(() => {
      this.apiPath = this._cfgSrvc.getApiPath();
      console.log('API Path: ', this.apiPath);
    });
  }

  public getActivities() {
    return this._http.get(this.apiPath + 'activities');
  }

}
