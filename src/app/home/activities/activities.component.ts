import { ActivityModel } from './../../models/activity.model';
import { ActivitiesService } from './activities.service';
import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {

  public activities: ActivityModel[];

  constructor(
    private _activitiesService: ActivitiesService
  ) {
    this._activitiesService.getActivities().subscribe(res => {
      this.activities = res as ActivityModel[];
    });
    this.startTimer();
  }

  ngOnInit() {
  }

  private startTimer() {
    const obs = interval(5000);
    obs.subscribe(x => {
      this._activitiesService.getActivities().subscribe(res => {
        this.activities = res as ActivityModel[];
      });
    });
  }

}
