import { UserGroupModel } from './../../models/usergroup.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { ConfigFileService } from 'src/app/services/config-file.service';
import { UserGroupService } from './user-group.service';
import { UserGroupDialogComponent } from './user-group-dialog/user-group-dialog.component';
import { SnackbarService, EMode } from 'src/app/snackbar.service';

export interface DialogData {
  userGroup: UserGroupModel;
}

@Component({
  selector: 'app-user-groups',
  templateUrl: './user-groups.component.html',
  styleUrls: ['./user-groups.component.css']
})
export class UserGroupsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'accesslevel'];
  public userGroupList: MatTableDataSource<UserGroupModel>;
  public userTotal: number;
  public userGroupsTotal: number;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private _cfgSrvc: ConfigFileService,
    private _userGroupService: UserGroupService,
    private _snackbarService: SnackbarService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(res => {
      setTimeout(() => {
        this._userGroupService.getUserGroups().subscribe(groups => {
          const tempGroupList = groups as UserGroupModel[];
          this.userGroupList = new MatTableDataSource(tempGroupList);
          this.userGroupList.sort = this.sort;
          this.setGroupsCardHeight(this.userGroupList.data.length);
        });
      }, 50);
    });
  }

  ngOnInit() {

  }

  public filterUserGroupList(criteria: string) {
    this.userGroupList.filter = criteria.trim().toLowerCase();

    const newTotal = this.userGroupList.data.length;
    if (this.userGroupsTotal !== newTotal) {
      this.userTotal = newTotal;
    } else if (!criteria) {
      this.userGroupsTotal = -1;
    }
    this.setGroupsCardHeight(this.userGroupList.filteredData.length);
  }

  public openUserGroupDialog(lm: UserGroupModel): void {
    let userGroup: UserGroupModel = new UserGroupModel();
    if (lm) {
      userGroup = lm;
    }
    const dialogRef = this.dialog.open(UserGroupDialogComponent, {
      data: { userGroup: userGroup }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('result: ', result)
      const tempUserGroup: UserGroupModel = result;
      if (tempUserGroup.id && !tempUserGroup.access_level && !tempUserGroup.name) {
        console.log('delete user');
        this.deleteUserGroup(tempUserGroup.id);
      } else {
        console.log('save user');
        this.saveUserGroup(tempUserGroup);
      }
    });
  }

  private saveUserGroup(userGroup: UserGroupModel) {
    this._userGroupService.setUserGroup(userGroup).subscribe(result => {
      if (userGroup.id) {
        this._snackbarService.create('Nutzerdaten erfolgreich gespeichert.', 'Ok', EMode.info);
      } else {
        console.log('result id: ', result[0].id);
        userGroup.id = result[0].id;
        this.userGroupList.data.push(userGroup);
        this.setGroupsCardHeight(this.userGroupList.data.length);
        this._snackbarService.create('Nutzer erfolgreich hinzugefügt.', 'Ok', EMode.info);
      }
    }, error => {
      this._snackbarService.create('Speichern fehlgeschlagen!', 'Ok', EMode.warn);
    });
  }

  private deleteUserGroup(id: number) {
    this._userGroupService.deleteGroup(id).subscribe(() => {
      this._snackbarService.create('Nutzer erfolgreich gelöscht.', 'Ok', EMode.info);
    }, error => {
      this._snackbarService.create('Löschen fehlgeschlagen!', 'Ok', EMode.warn);
    });
  }


  private setGroupsCardHeight(displayedRows: number) {
    setTimeout(() => {
      const lockcard = document.getElementById('groups-card-identifier');
      const tablecontainer = document.getElementById('groups-table-container');

      let lli_offsetHeight = 0;

      if (displayedRows > 12) {
        lli_offsetHeight = 650;
        tablecontainer.style.overflow = 'auto';
      } else {
        lli_offsetHeight = (displayedRows * 48) + 56;
        tablecontainer.style.overflow = 'hidden';
      }

      tablecontainer.style.height = (lli_offsetHeight).toString() + 'px';
      this.userGroupList.sort = this.sort;
    }, 100);
  }

}
