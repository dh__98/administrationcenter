import { ConfigFileService } from './../../services/config-file.service';
import { UserGroupModel } from './../../models/usergroup.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserGroupService {

  private apiPath: string;

  constructor(
    private _http: HttpClient,
    private _cfgSrvc: ConfigFileService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(() => {
      this.apiPath = this._cfgSrvc.getApiPath();
    });
  }

  public getUserGroups() {
    return this._http.get(this.apiPath + 'user_groups');
  }

  public setUserGroup(userGroup: UserGroupModel) {
    return this._http.post(this.apiPath + 'user_groups/update', userGroup);
  }

  public deleteGroup(id: number) {
    return this._http.delete(this.apiPath + 'user_groups/delete?id=' + id);
  }
}
