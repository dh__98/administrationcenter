import { DialogData } from './../../user-groups/user-groups.component';
import { ModularDialogComponent } from './../../../dialogs/modular-dialog/modular-dialog.component';
import { ModularData } from 'src/app/dialogs/modular-dialog/modular-dialog.component';
import { SnackbarService, EMode } from './../../../snackbar.service';

import { UserGroupModel } from './../../../models/usergroup.model';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-user-group-dialog',
  templateUrl: './user-group-dialog.component.html',
  styleUrls: ['./user-group-dialog.component.css']
})
export class UserGroupDialogComponent implements OnInit {

  public userGroup: UserGroupModel;
  public tempName: string;
  public levels: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  public isNew: boolean;

  constructor(
    public dialogRef: MatDialogRef<UserGroupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackbarService: SnackbarService,
    public dialog: MatDialog
  ) {
    if (data.userGroup.id) {
      this.userGroup = {
        access_level: data.userGroup.access_level,
        id: data.userGroup.id,
        name: data.userGroup.name
      };
    } else {
      this.userGroup = new UserGroupModel();
      this.isNew = true;
    }
  }

  public setAccessLevel(level) {
    this.userGroup.access_level = level;
  }

  public saveGroup() {
    if (this.userGroup) {
      if (!this.userGroup.access_level || !this.userGroup.name) {
        this.snackbar();
      } else {
        this.dialogRef.close(this.userGroup);
      }
    } else {
      this.snackbar();
    }
  }

  private snackbar() {
    this._snackbarService.create('Bitte alle Datenfelder füllen.', 'Ok', EMode.info);
  }

  public cancel() {
    this.dialogRef.close();
  }

  public deleteGroup() {
    const modularData = new ModularData('Löschen', 'Alle Verweise auf Nutzern werden entfernt, das Berechtigungslevel bleibt.' +
      ' \r\n Wirklich löschen?', 'Abbrechen', 'Ja');

    const dialogRef = this.dialog.open(ModularDialogComponent, {
      data: { modularData: modularData }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const group: UserGroupModel = {
          access_level: null,
          id: this.userGroup.id,
          name: null
        }
        this.dialogRef.close(group);
      }
    });
  }

  ngOnInit() {

  }

}
