import { UserModel } from './../../models/user.model';
import { ConfigFileService } from './../../services/config-file.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private apiPath: string;

  constructor(
    private _http: HttpClient,
    private _cfgSrvc: ConfigFileService
  ) {
    this._cfgSrvc.loadConfigFile().subscribe(() => {
      this.apiPath = this._cfgSrvc.getApiPath();
      console.log('API Path: ', this.apiPath);
    });
  }

  public getUsers() {
    return this._http.get(this.apiPath + 'users');
  }

  public getUserByID(id: number) {
    return this._http.get(this.apiPath + 'users?id=' + id);
  }

  public setUser(user: UserModel) {
    return this._http.post(this.apiPath + 'users/update', user);
  }

  public deleteUser(id: number) {
    return this._http.delete(this.apiPath + 'users/delete?id=' + id);
  }

  public getUserGroups() {
    return this._http.get(this.apiPath + 'user_groups');
  }

  public getLocks(user_taguid: string) {
    return this._http.get(this.apiPath + 'getuseraccessiblelocks?taguid=' + user_taguid);
  }

  

}
