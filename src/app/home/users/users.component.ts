import { LockModel } from './../../models/lock.model';
import { UserGroupModel } from './../../models/usergroup.model';

import { UserModel } from './../../models/user.model';
import { UsersService } from './users.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { filterAnimation, flyInAnimation } from 'src/app/animations/animations';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { ConfigFileService } from 'src/app/services/config-file.service';
import { SnackbarService, EMode } from 'src/app/snackbar.service';

export interface DialogData {
  user: UserModel;
  userGroups: Array<UserGroupModel>;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  animations: [
    filterAnimation,
    flyInAnimation
  ]
})
export class UsersComponent implements OnInit, AfterViewInit {

  displayedColumnsLocks: string[] = ['id', 'description', 'accesslevel'];

  public criteria: string;
  public userList: Array<UserModel>;
  private userListOG: Array<UserModel>;
  public userTotal: number;
  public locksTotal: number;
  public currentUser: UserModel;
  private _currentUserOG: UserModel;
  public currUser_userGroup: string;
  public newUser: UserModel;
  public lockList: MatTableDataSource<LockModel>;
  public userGroups: Array<UserGroupModel>;

  public filteredLockListLength: number;
  public lockListActivePage: number;

  public name: string;
  public lastName: string;
  public tagUID: string;
  public confirmed: boolean;
  public accessLevel: number;

  private lastClicked: any = null;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private _userService: UsersService,
    public dialog: MatDialog,
    private _cfgSrvc: ConfigFileService,
    private _snackbarService: SnackbarService
  ) {

    this._cfgSrvc.loadConfigFile().subscribe(res => {
      setTimeout(() => {
        this._userService.getUsers().subscribe(users => {
          this.userListOG = users as Array<UserModel>;
          this._userService.getUserGroups().subscribe(groups => {
            this.userGroups = groups as Array<UserGroupModel>;
            console.log('this.userGroups: ', this.userGroups.length);
            this.setUserListHeight();
            this.filterUserList('');
            if (this.userListOG.length > 0) {
              this.openUserDetail(this.userListOG[0].id);
            }
          }, error => {
            this._snackbarService.create('Nutzergruppen laden fehlgeschlagen.', 'Ok', EMode.warn);
          });
        }, error => {
          this._snackbarService.create('Nutzer laden fehlgeschlagen.', 'Ok', EMode.warn);
        });
      }, 50);
    });
  }

  ngOnInit() { }

  ngAfterViewInit() { }

  public filterUserList(criteria: string) {
    this.userList = this.userListOG.filter(user =>
      user.name.toLowerCase().includes(criteria.toLowerCase()) || user.id.toString().includes(criteria)
    );
    const newTotal = this.userList.length;
    if (this.userTotal !== newTotal) {
      this.userTotal = newTotal;
    } else if (!criteria) {
      this.userTotal = -1;
    }
    this.setUserListHeight();
  }

  public openUserDetail(id: number) {
    if (!this.lastClicked && Date.now() - this.lastClicked < 500) {
      console.log(Date.now() - this.lastClicked)
      this.lastClicked = Date.now();
      return;
    }

    this.currentUser = null;
    // if (this.lockList) {
    //   this.lockList.data = null;
    // }

    setTimeout(() => {
      if (id) {
        this.currentUser = this.userListOG.find(user => user.id === id);
        console.log('fku', this.userGroups);
        const temp = this.userGroups.find(group => group.id === this.currentUser.user_group_id);
        if (temp) {
          this.currentUser.user_group_name = temp.name;
        }

        this.GetAccessibleLocks();
        console.log('currentUser: ', this.currentUser);
      } else {
        this.currentUser = new UserModel();
      }
    }, 500);
  }

  private GetAccessibleLocks() {
    this._userService.getLocks(this.currentUser.taguid).subscribe(locks => {
      const tempLockList = locks as Array<LockModel>;
      this.lockList = new MatTableDataSource(tempLockList);
      this.setUserCardHeigth(this.lockList.data.length);
    }, error => {
      this._snackbarService.create('Laden der berechtigten Schlösser des Nutzers fehlgeschlagen.', 'Ok', EMode.warn);
    });
  }

  public openUserDialog(statement: string): void {
    let user: UserModel = new UserModel();
    if (statement !== 'new') {
      user = this.currentUser;
      this._currentUserOG = this.currentUser;
    }

    const dialogRef = this.dialog.open(UserDialogComponent, {
      data: { user: user, userGroups: this.userGroups }
    });

    dialogRef.afterClosed().subscribe(result => {
      const tempUser: UserModel = result;
      if (tempUser) {
        console.log(tempUser);
        if (tempUser.id && !tempUser.accesslevel && !tempUser.birthdate && !tempUser.lastname && !tempUser.name && !tempUser.taguid) {
          this.deleteUser(tempUser.id);
        } else {
          this.saveUser(tempUser);
        }
      }
    });
  }

  private saveUser(user: UserModel) {
    this._userService.setUser(user).subscribe(result => {
      if (user.id) {
        this._snackbarService.create('Nutzerdaten erfolgreich gespeichert.', 'Ok', EMode.info);
        this.currentUser = user;
      } else {
        console.log('result id: ', result[0].id);
        user.id = result[0].id;
        this.userListOG.push(user);
        this.userList.push(user);
        this.setUserListHeight();
        this._snackbarService.create('Nutzer erfolgreich hinzugefügt.', 'Ok', EMode.info);
      }
      this.GetAccessibleLocks();
    }, error => {
      this._snackbarService.create('Speichern fehlgeschlagen!', 'Ok', EMode.warn);
      this.currentUser = this._currentUserOG;
    });
  }

  private deleteUser(id: number) {
    this._userService.deleteUser(id).subscribe(() => {
      this._snackbarService.create('Nutzer erfolgreich gelöscht.', 'Ok', EMode.info);
    }, error => {
      this._snackbarService.create('Löschen fehlgeschlagen!', 'Ok', EMode.warn);
    });
  }

  public filterLocksList(criteria: string) {
    this.lockList.filter = criteria.trim().toLowerCase();
    if (this.lockList.paginator) {
      this.lockList.paginator.firstPage();
    }
    const newTotal = this.lockList.data.length;
    if (this.locksTotal !== newTotal) {
      this.userTotal = newTotal;
    } else if (!criteria) {
      this.locksTotal = -1;
    }
    this.setUserCardHeigth(this.lockList.filteredData.length);
  }

  private setUserCardHeigth(displayedRows: number) {
    setTimeout(() => {
      const usercard = document.getElementById('usercard-identifier');
      const usercard_header = document.getElementById('usercard-header-identifier');
      const usercard_text = document.getElementById('usercard-text-identifier');

      let lli_offsetHeight = 0;

      if (displayedRows > 5) {
        lli_offsetHeight = 417;
      } else {
        lli_offsetHeight = 65 + 56 + (displayedRows * 48) + 56;
      }

      usercard.style.height = (usercard_header.clientHeight + usercard_text.clientHeight + lli_offsetHeight + 55).toString() + 'px';
      this.lockList.paginator = this.paginator;
      this.lockList.sort = this.sort;
    }, 100);
  }

  private setUserListHeight() {
    setTimeout(() => {
      const userlist = document.getElementById('userlist-identifier');
      const userlist_search = document.getElementById('userlist-search-identifier');
      const userlist_add = document.getElementById('userlist-add-identifier');
      const userlist_header = document.getElementById('userlist-tableheader-identifier');
      const custom_userlist = document.getElementById('custom-mat-userlist');

      let items_height = 0;

      for (let index = 0; index < this.userListOG.length; index++) {
        const userlist_item = document.getElementById(`userlist-item-identifier-${index}`);
        items_height += userlist_item.offsetHeight;
      }

      if (items_height > 575) {
        custom_userlist.style.overflowY = 'scroll';
        items_height = 560;
      } else {
        custom_userlist.style.overflowY = 'hidden';
      }

      userlist.style.height = (userlist_search.clientHeight + userlist_add.clientHeight +
        userlist_header.clientHeight + items_height + 65).toString() + 'px';
    }, 100);
  }

  public paginatorPageChanged() {
    if ((this.lockList.paginator.pageIndex + 1) * 5 > this.lockList.data.length) {
      this.setUserCardHeigth(this.lockList.data.length % 5);
    } else {
      this.setUserCardHeigth(5);
    }
  }

}
