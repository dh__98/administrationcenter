import { UsersService } from './../users.service';
import { SnackbarService, EMode } from './../../../snackbar.service';
import { DialogData } from './../users.component';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { UserModel } from 'src/app/models/user.model';
import { ModularDialogComponent, ModularData } from 'src/app/dialogs/modular-dialog/modular-dialog.component';
import { UserGroupModel } from 'src/app/models/usergroup.model';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface ModularDialogData {
  modularData: ModularData;
}

@Component({
  selector: 'app-user-dialog',
  templateUrl: './user-dialog.component.html',
  styleUrls: ['./user-dialog.component.css']
})
export class UserDialogComponent implements OnInit {

  public user: UserModel;
  public tempName: string;
  public levels: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  public isNew: boolean;
  public userGroups: Array<UserGroupModel>;
  public filteredUserGroups: Observable<Array<UserGroupModel>>;
  public control: FormControl;

  constructor(
    public dialogRef: MatDialogRef<UserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialog: MatDialog,
    private _snackbarService: SnackbarService,
    private _userService: UsersService
  ) {
    this.userGroups = data.userGroups;
    if (data.user.id) {
      //data.user
      this.user = {
        accesslevel: data.user.accesslevel,
        birthdate: data.user.birthdate,
        id: data.user.id,
        lastname: data.user.lastname,
        name: data.user.name,
        taguid: data.user.taguid,
        user_group_id: data.user.user_group_id,
        user_group_name: data.user.user_group_name
      };
      this.control = new FormControl(this.user.user_group_name);
    } else {
      this.user = new UserModel();
      this.user.user_group_id = null;
      this.user.user_group_name = null;
      this.isNew = true;
      this.control = new FormControl();
    }
  }

  ngOnInit() {
    this.filteredUserGroups = this.control.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): Array<UserGroupModel> {
    const filterValue = value.toLowerCase();

    return this.userGroups.filter(option => option.name.toLowerCase().includes(filterValue) || option.id.toString().includes(filterValue));
  }

  public selectedUserGroup(group: UserGroupModel) {
    console.log('group: ', group)
    if (group) {  
      this.user.user_group_id = group.id;
      this.user.user_group_name = group.name;
      this.user.accesslevel = group.access_level;
      
    }
  }

  public setAccessLevel(level) {
    this.user.accesslevel = level;
  }

  public saveUser() {
    if (!this.user.accesslevel || !this.user.birthdate || !this.user.lastname || !this.user.name || !this.user.taguid) {
      this._snackbarService.create('Bitte alle Datenfelder füllen.', 'Ok', EMode.info);
    } else {
      this.dialogRef.close(this.user);
    }
  }

  public deleteUser() {
    const modularData = new ModularData('Löschen', 'Wollen Sie den Nutzer wirklich löschen?', 'Abbrechen', 'Löschen');

    const dialogRef = this.dialog.open(ModularDialogComponent, {
      data: { modularData: modularData }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        const user: UserModel = {
          id: this.user.id,
          name: null,
          lastname: null,
          taguid: null,
          accesslevel: null,
          birthdate: null,
          user_group_id: null,
          user_group_name: null
        };
        this.dialogRef.close(user);
      }
    });
  }

  public cancel() {
    this.dialogRef.close();
  }

}
