import { ConfigFileModel } from '../models/config-file.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConfigFileService {

  private _config: ConfigFileModel;

  constructor(
    private _http: HttpClient
  ) { }

  public loadConfigFile() {
    const observable = new Observable<boolean>(observer => {
      return this._http.get('../assets/config.json').subscribe(config => {
        this._config = config as ConfigFileModel;
        observer.next(true);
      }, error => {
        observer.error(error);
      });
    });
    return observable;
  }

  public getApiPath() {
    if (this._config) {
      return this._config.api_path;
    } else {
      return '';
    }
  }
}
