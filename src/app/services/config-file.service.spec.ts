import { TestBed } from '@angular/core/testing';

import { ConfigFileService } from './config-file.service';

describe('ConfigFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigFileService = TestBed.get(ConfigFileService);
    expect(service).toBeTruthy();
  });
});
