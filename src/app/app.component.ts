import { ModularData, ModularDialogComponent } from './dialogs/modular-dialog/modular-dialog.component';
import { InfoDialogComponent } from './dialogs/info-dialog/info-dialog.component';
import { SettingsDialogComponent } from './dialogs/settings-dialog/settings-dialog.component';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { fadeAnimation } from './animations/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    fadeAnimation
  ]
})
export class AppComponent {

  title = 'Administration Center';

  constructor(
    private _router: Router,
    public dialog: MatDialog

  ) {
    this._router.navigateByUrl('login');
  }

  public openSettingsDialog() {
    const dialogRef = this.dialog.open(SettingsDialogComponent, {

    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  public openLogoutDialog() {
    const modularData = new ModularData('Logout', 'Wollen Sie sich wirklich ausloggen?', 'Abbrechen', 'Ja');
    
    const dialogRef = this.dialog.open(ModularDialogComponent, {
      data: { modularData: modularData }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this._router.navigateByUrl('login');
      }
    });
  }

  public openInfoDialog() {
    const dialogRef = this.dialog.open(InfoDialogComponent, {

    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

}
